#!/usr/bin/env bash

_current_dir="$( cd $(dirname "$0"); pwd -P )"


tmux source-file "$_current_dir/my-theme.conf"

if [[ -n $SSH_CLIENT ]]; then
  tmux source-file "$_current_dir/my-theme.remote.conf"
else
 tmux source-file "$_current_dir/my-theme.local.conf"
fi
