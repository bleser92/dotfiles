#!/usr/bin/env bash

set -e

scriptPath="$( cd $(dirname "$0"); pwd -P )"

source "$scriptPath/utils.sh"


if [[ "$(hash zsh 2> /dev/null)" -ne 0 ]]; then
  echo "zsh not installed" >&2
  exit 1
fi

zsh_cfg_dir="$HOME/.zsh"

createDir $zsh_cfg_dir

ln -sf "$scriptPath"/zsh/* $zsh_cfg_dir

touch $zsh_cfg_dir/.zsh_history
touch $zsh_cfg_dir/.zshenv

ln -s --backup=t $zsh_cfg_dir/zshrc $HOME/.zshrc
ln -s --backup=t $zsh_cfg_dir/.zshenv $HOME/.zshenv

