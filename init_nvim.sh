#!/usr/bin/env bash

set -e

scriptPath="$( cd $(dirname "$0"); pwd -P )"

source "$scriptPath/utils.sh"

if [[ "$(hash nvim 2> /dev/null)" -ne 0 ]]; then
  echo "Neovim not installed" >&2
  exit 1
fi

if [[ "$(hash git 2> /dev/null)" -ne 0 ]]; then
  echo "git not installed" >&2
  exit 1
fi


nvim_cfg_dir="$HOME/.config/nvim"

createDir $nvim_cfg_dir

ln -s "$scriptPath"/nvim/* $nvim_cfg_dir


if [[ ! -d "$HOME/.local/share/nvim/site/pack/packer/start/packer.nvim" ]]; then
  echo "Install Packer"

  git clone --depth 1 https://github.com/wbthomason/packer.nvim \
    "$HOME/.local/share/nvim/site/pack/packer/start/packer.nvim"

  echo "Packer successful installed"

  echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
  echo "| Run :PackerInstall :PackerCompile for install plugins |"
  echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
else
  echo "Nvim already inited"
fi


