#!/usr/bin/env bash

set -e

scriptPath="$( cd $(dirname "$0"); pwd -P )"

source "$scriptPath/utils.sh"

if [[ "$(hash tmux 2> /dev/null)" -ne 0 ]]; then
  echo "Tmux not installed" >&2
  exit 1
fi

if [[ "$(hash git 2> /dev/null)" -ne 0 ]]; then
  echo "git not installed" >&2
  exit 1
fi


tmux_cfg_dir="$HOME/.config/tmux"

createDir $tmux_cfg_dir


ln -s "$scriptPath"/tmux/* $tmux_cfg_dir


# if [[ ! -d  "$HOME/.config/tmux/plugins/tpm" ]]; then
#   git clone https://github.com/tmux-plugins/tpm \
#     "$HOME/.config/tmux/plugins/tpm"
#
#   echo "++++++++++++++++++++++++++++++++++++++++++++++++++++"
#   echo "| tmux init complete                               |"
#   echo "| run tmux and type <PREFIX> I for install plugins |"
#   echo "++++++++++++++++++++++++++++++++++++++++++++++++++++"
# else
#   echo "tmux already inited"
# fi


