#!/bin/bash


set -e

scriptPath="$( cd $(dirname "$0"); pwd -P )"

source "$scriptPath/utils.sh"

if [[ "$(hash alacritty 2> /dev/null)" -ne 0 ]]; then
  echo "Alacritty not installed" >&2
  exit 1
fi



alacritty_cfg_dir="$HOME/.config/alacritty"

if [[ -d "$alacritty_cfg_dir" ]]; then
  echo "Alacritty config directory($alacritty_cfg_dir) already exists." >&2
  exit 1
fi

#createDir "$alacritty_cfg_dir"

ln -s "$scriptPath"/alacritty $alacritty_cfg_dir

