#!/bin/bash

set -e

scriptPath="$( cd $(dirname "$0"); pwd -P )"

source "$scriptPath/utils.sh"

if [[ "$(hash helix 2> /dev/null)" -ne 0 ]]; then
  echo "Helix not installed" >&2
  exit 1
fi

helix_config_dir="$HOME/.config/helix"

if [[ -d "$helix_config_dir" ]]; then
  request_result="$(requestUser "Helix config dir($helix_config_dir) exists. Backup and replace it?")"
  if [[ $request_result == "0" ]]; then
    backupDir "$helix_config_dir"
  else
    exit 0
  fi
fi


ln -s "$scriptPath/helix/" "$helix_config_dir"
