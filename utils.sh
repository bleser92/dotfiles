


createDir() {
  if [[ -d "$1" ]]; then
    mv --backup=t "$1" "$1.backup"
  fi

  if [[ ! -d "$1" ]]; then
    echo "Create config dir: $1"
    mkdir -p "$1"
  fi

  if [[ "$(ls -A $1)" ]]; then
    echo "$1 dir is not empty" >&2
    exit 1
  fi
}

backupDir() {
  if [[ -d "$1" ]]; then
    mv -v --backup=t "$1" "$1.backup"
  fi
}


requestUser() {
  shopt -s nocasematch

  while true; do
    read -p "$1 [y/N]: " user_select
    result=1

    case "$user_select" in
      "y") 
        result=0
        break
      ;;
      "n")
        break
      ;;
      "") 
        break
      ;;
      *) continue;;
    esac
  done

  echo $result
}
